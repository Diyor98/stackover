from django.db import models

import os
from uuid import uuid4


def get_question_image_path(instance, filename):
    ext = str(filename).split('.')[-1]
    filename = f'{uuid4()}.{ext}'
    return os.path.join('questions/', filename)


class User(models.Model):
    name = models.CharField(max_length=255)


class Question(models.Model):
    question = models.TextField()
    image = models.ImageField(upload_to=get_question_image_path)
    upvotes = models.IntegerField(default=0)
    user_name = models.ForeignKey(User, on_delete=models.CASCADE)


class Answer(models.Model):
    answer = models.TextField()
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    user_name = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('user_name', 'question')