from rest_framework.routers import SimpleRouter
from django.urls import path, include

from stack.views import (
    QuestionModelViewSet,
    AnswerModelViewSet
)

router = SimpleRouter()
router.register('questions', QuestionModelViewSet)
router.register('answers', AnswerModelViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
