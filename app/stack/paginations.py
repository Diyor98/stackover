from rest_framework.pagination import (
    LimitOffsetPagination,
    PageNumberPagination,
)


class DefaultPageNumberPagination(PageNumberPagination):
    page_size = 10


class DefaultLimitOffsetPagination(LimitOffsetPagination):
    default_limit = 5
    max_limit = 50
