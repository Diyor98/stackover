# Generated by Django 3.0.8 on 2020-07-22 05:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stack', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='upvotes',
            field=models.IntegerField(default=0),
        ),
    ]
