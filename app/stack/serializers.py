from stack.models import Question, Answer
from rest_framework.serializers import ModelSerializer, StringRelatedField

class QuestionSerializer(ModelSerializer):
    class Meta:
        model = Question
        fields = '__all__'

class AnswerSerializer(ModelSerializer):
    class Meta:
        model = Answer
        fields = '__all__'